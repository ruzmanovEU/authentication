import os
from core.hashing import Hasher
from core.otp import OTP
from database.models.users import User
from database.repository.users import get_user_by_email, create_token
from database.session import get_db
from fastapi import APIRouter
from fastapi import Depends
from fastapi import HTTPException
from fastapi import Request
from fastapi.templating import Jinja2Templates
from sqlalchemy.orm import Session
from webapps.login.forms import LoginForm
from secrets import compare_digest
from fastapi_mail import FastMail, MessageSchema, ConnectionConfig

templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)


class Envs:
    MAIL_USERNAME = os.getenv('MAIL_USERNAME')
    MAIL_PASSWORD = os.getenv('MAIL_PASSWORD')
    MAIL_FROM = os.getenv('MAIL_FROM')
    MAIL_PORT = int(os.getenv('MAIL_PORT'))
    MAIL_SERVER = os.getenv('MAIL_SERVER')
    MAIL_FROM_NAME = os.getenv('MAIN_FROM_NAME')


conf = ConnectionConfig(
    MAIL_USERNAME=Envs.MAIL_USERNAME,
    MAIL_PASSWORD=Envs.MAIL_PASSWORD,
    MAIL_FROM=Envs.MAIL_FROM,
    MAIL_PORT=Envs.MAIL_PORT,
    MAIL_SERVER=Envs.MAIL_SERVER,
    MAIL_FROM_NAME=Envs.MAIL_FROM_NAME,
    MAIL_STARTTLS=True,
    MAIL_SSL_TLS=False,
    USE_CREDENTIALS=True,
    TEMPLATE_FOLDER='templates/email'
)


async def send_email_async(subject: str, email_to: str, body: dict):
    message = MessageSchema(
        subject=subject,
        recipients=[email_to],
        template_body=body,
        subtype='html',
    )

    fm = FastMail(conf)

    await fm.send_message(message, template_name='email.html')


@router.get("/login/")
async def login(request: Request):
    return templates.TemplateResponse("login/login.html", {"request": request})


@router.post("/login/")
async def login(request: Request, db: Session = Depends(get_db)):
    form = LoginForm(request)
    await form.load_data()
    if await form.is_valid():
        try:
            user: User = get_user_by_email(form.email, db=db)
            if user is None:
                form.__dict__.get("errors").append("Incorrect Credentails")
                return templates.TemplateResponse("login/login.html", form.__dict__)

            if form.token and user.secret == form.token:
                return templates.TemplateResponse(
                    "home/index.html",
                    {"request": request, "email": user.email},
                )

            elif not form.token:
                token = create_token(form.email, db=db)
                await send_email_async(
                    'Secret token',
                    form.email,
                    {
                        'title': 'Super secret token',
                        'token': token,
                    }
                )

                return templates.TemplateResponse(
                    "login/login_confirm.html",
                    {"request": request, "email": form.email, 'password': form.password},
                )

            else:
                raise HTTPException(status_code=403)

        except HTTPException:
            form.__dict__.update(msg="")
            form.__dict__.get("errors").append("Incorrect Email or Password")
            return templates.TemplateResponse("login/login.html", form.__dict__)
    return templates.TemplateResponse("login/login.html", form.__dict__)